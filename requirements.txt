# jupyter lab
jupyterlab

# qiskit
qiskit
qiskit[visualization]
qiskit_machine_learning

# sklearn
scikit-learn

# useful libs
numpy
scipy
pandas

# plotting
matplotlib
