# QM-2023-Tuto

This repository contains some basic file to do the tutorial presented during the Quantum Meets 2023 at Maastricht University


## Creating a virtual environment for Qiskit

For the tutorial, we will use [Qiskit](https://qiskit.org/) as our quantum computing library. The first step will be the download and install the needed libraries.

An easy, reproducible, non-intrusive, way to install libraries in Python is to use a virtual environment. You first needs to have Python installed. If you don't have Python, you can install a distribution using [Anaconda](https://www.anaconda.com/), [Miniconda](https://docs.conda.io/en/latest/miniconda.html) or [Mambaforge](https://github.com/conda-forge/miniforge#mambaforge).

### Create a directory for the project and extract the repository files there (or clone the repository)

```bash
git clone https://gitlab.com/jgobeil/qm-2023-tuto
cd qm-2023-tuto
```

### Create a virtual environment for the project. To do so, start a terminal in the project directory and call the `venv` module.
```bash
python -m venv env
```
That will create a virtual environement in the directory `env`.

### Activate that environement using the right command 

see https://docs.python.org/3/library/venv.html#how-venvs-work). 

On Windows
```bash
C:\> <venv>\Scripts\activate.bat
```

On MacOs or Linux
```bash
source <venv>/bin/activate
```
You should now see a `(env)` in you command line. 

### Install the requirements

```bash
pip install -r requirements.txt
```
You now have a fully functional environment for the tutorial

### Start Jupyter lab
```bash
jupyter lab
```

Et voila!



